use regex::{
    Regex,
    Captures,
};
use serde::Deserialize;
use std::{
    borrow::Cow,
    error::Error,
    fmt,
};

#[derive(Debug, Deserialize, Clone)]
#[serde(untagged)]
pub enum CapturedSegment {
    Literal(String),
    Capture(usize),
}

impl CapturedSegment {
    fn displayed<'a>(&'a self, caps: &'a Captures<'a>) -> &'a str {
        match self {
            CapturedSegment::Literal(s) => s,
            CapturedSegment::Capture(idx) => &caps[*idx],
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
#[serde(untagged)]
pub enum ReplaceValue {
    Literal(String),
    Captured(Vec<CapturedSegment>),
}

#[derive(Debug, Deserialize, Clone)]
pub enum CmdOperation {
    Prefix(Vec<String>),
    Suffix(Vec<String>),
    Replace {
        pattern: String,
        value: ReplaceValue,
    },
    Remove {
        pattern: String,
    },
    ReplaceAll(Vec<String>),
}

impl CmdOperation {
    pub fn apply<'a>(&self, mut command: Vec<Cow<'a, str>>) -> Result<Vec<Cow<'a, str>>, Box<dyn Error>> {
        match self {
            CmdOperation::Prefix(values) => {
                let mut ret: Vec<_> = values.iter()
                    .map(|s| Cow::Owned(s.clone()))
                    .collect();
                ret.extend(command.into_iter());
                Ok(ret)
            },
            CmdOperation::Suffix(values) => {
                command.extend(values.clone().into_iter().map(Cow::Owned));
                Ok(command)
            },
            CmdOperation::Replace { pattern, value } => {
                let pattern = Regex::new(pattern.as_ref())?;
                let new_command = command.into_iter().map(|v| {
                    if pattern.is_match(&v) {
                        Cow::Owned(pattern.replace(&v, |caps: &Captures| {
                            use fmt::Write;
                            match value {
                                ReplaceValue::Literal(s) => s.clone(),
                                ReplaceValue::Captured(segments) => {
                                    let mut buf = "".to_owned();
                                    segments.iter()
                                        .map(|s| s.displayed(caps))
                                        .try_for_each(|s| write!(&mut buf, "{}", s))
                                        .unwrap();
                                    buf
                                },
                            }
                        }).into_owned())
                    } else {
                        v
                    }
                });
                Ok(new_command.collect())
            },
            CmdOperation::Remove { pattern } => {
                let pattern = Regex::new(pattern.as_ref())?;
                let new_command = command.into_iter().filter(|v| !pattern.is_match(v));
                Ok(new_command.collect())
            },
            CmdOperation::ReplaceAll(new_command) => Ok(
                new_command.iter()
                    .map(|s| Cow::Owned(s.clone()))
                    .collect()
            ),
        }
    }
}
