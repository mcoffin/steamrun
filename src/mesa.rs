use std::{
    env,
    io,
};
use serde::Deserialize;
use crate::{
    ApplicableConfig,
    env_ext,
    slice_ext,
};

pub const OVERLAY_LAYERS: &'static [&'static str] = &["VK_LAYER_MESA_overlay"];

#[derive(Debug, Clone, Deserialize)]
pub struct MesaConfig {
    vulkan_overlay: Option<String>,
    gallium_hud: Option<String>,
}

impl MesaConfig {
    #[inline(always)]
    fn overlay_layers() -> impl Iterator<Item=&'static str> {
        slice_ext::slice_iter(OVERLAY_LAYERS)
    }
}

impl ApplicableConfig for MesaConfig {
    fn steamrun_apply(&self) -> io::Result<()> {
        if let &Some(ref vk_overlay_config) = &self.vulkan_overlay {
            env_ext::ListDescriptor::new("VK_INSTANCE_LAYERS", Some(":"))
                .append_if_not_contains(Self::overlay_layers());
            env::set_var("VK_LAYER_MESA_OVERLAY_CONFIG", vk_overlay_config);
        }

        if let Some(gallium_hud_config) = self.gallium_hud.as_ref() {
            env::set_var("GALLIUM_HUD", gallium_hud_config);
        }

        Ok(())
    }
}

impl Default for MesaConfig {
    #[inline(always)]
    fn default() -> Self {
        MesaConfig {
            vulkan_overlay: None,
            gallium_hud: None,
        }
    }
}
