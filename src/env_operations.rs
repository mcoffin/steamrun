use std::{
    env,
    io,
};
use serde::Deserialize;
use crate::{
    env_ext::{
        self,
        ExtendPosition,
    },
    ApplicableConfig,
};

// #[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
// #[serde(rename_all = "lowercase")]
// pub enum EnvOperationType {
//     Append,
//     Prepend,
// }
// 
// impl EnvOperationType {
//     pub const fn to_position(self) -> env_ext::ExtendPosition {
//         use env_ext::ExtendPosition::*;
//         match self {
//             EnvOperationType::Append => After,
//             EnvOperationType::Prepend => Before,
//         }
//     }
// }
// 
// impl Into<env_ext::ExtendPosition> for EnvOperationType {
//     #[inline(always)]
//     fn into(self) -> env_ext::ExtendPosition {
//         self.to_position()
//     }
// }

#[derive(Debug, Deserialize, Clone)]
pub struct ListOperation {
    key: String,
    values: Vec<String>,
    separator: Option<String>,
}

impl ListOperation {
    #[inline]
    pub fn values<'a>(&'a self) -> impl Iterator<Item=&'a str> {
        self.values
            .iter()
            .map(AsRef::as_ref)
    }

    #[inline(always)]
    pub fn separator(&self) -> Option<&str> {
        self.separator.as_ref()
            .map(AsRef::as_ref)
    }
}

#[derive(Debug, Deserialize, Clone)]
#[serde(tag = "op", rename_all = "lowercase")]
pub enum EnvOperation {
    Append(ListOperation),
    Prepend(ListOperation),
    Remove {
        key: String,
    },
    Default {
        key: String,
        value: String,
    },
    Set {
        key: String,
        value: String,
    }
}

impl EnvOperation {
    #[inline(always)]
    pub fn key(&self) -> &str {
        use EnvOperation::*;
        match self {
            Append(config) => config.key.as_ref(),
            Prepend(config) => config.key.as_ref(),
            Remove { key } => key.as_ref(),
            Default { key, .. } => key.as_ref(),
            Set { key, .. } => key.as_ref(),
        }
    }
}

#[inline(always)]
fn get_var<K: AsRef<std::ffi::OsStr>>(key: K) -> Option<String> {
    env::var(key).ok()
}

impl ApplicableConfig for EnvOperation {
    fn steamrun_apply(&self) -> io::Result<()> {
        trace!("operation: {:?} - Before: {:?}", self, get_var(self.key()));
        match self {
            EnvOperation::Append(config) => env_ext::extend_env(&config.key, config.values(), ExtendPosition::After, config.separator()),
            EnvOperation::Prepend(config) => env_ext::extend_env(&config.key, config.values(), ExtendPosition::Before, config.separator()),
            EnvOperation::Remove { key } => env::remove_var(key),
            EnvOperation::Default { key, value } => {
                if env::var_os(key).filter(|s| s.len() > 0).is_none() {
                    env::set_var(key, value);
                }
            },
            EnvOperation::Set { key, value } => env::set_var(key, value),
        }
        trace!("operation: {:?} - After: {:?}", self, get_var(self.key()));
        Ok(())
    }
}
