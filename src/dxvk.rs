use crate::ApplicableConfig;
use std::{
    env,
    io,
};
use serde::{
    Deserialize,
};

#[derive(Debug, Deserialize, Clone)]
pub struct DxvkConfig {
    filter_device_name: Option<String>,
    hud: Option<String>,
}

impl DxvkConfig {
    #[inline(always)]
    pub fn filter_device_name(&self) -> Option<&str> {
        self.filter_device_name.as_ref().map(AsRef::as_ref)
    }

    pub fn hud(&self) -> Option<&str> {
        self.hud.as_ref()
            .filter(|s| {
                let s: &str = s.as_ref();
                s.len() > 0
            })
            .map(AsRef::as_ref)
    }
}

impl ApplicableConfig for DxvkConfig {
    fn steamrun_apply(&self) -> io::Result<()> {
        if let Some(filter) = self.filter_device_name() {
            env::set_var("DXVK_FILTER_DEVICE_NAME", filter);
        }
        if let Some(v) = self.hud() {
            env::set_var("DXVK_HUD", v);
        }
        Ok(())
    }
}
