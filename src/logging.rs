use std::{
    borrow::Cow,
    env,
    ffi::OsStr,
    sync::Once,
};
use env_logger_config::*;

#[allow(dead_code)]
fn env_or_default<K, V>(key: K, value: V) where
    K: AsRef<OsStr>,
    V: AsRef<OsStr>,
{
    let key = key.as_ref();
    if env::var_os(key).is_none() {
        env::set_var(key, value);
    }
}

const DEFAULT_LOG_LEVEL: log::Level = log::Level::Info;
const CRATE_NAME: &'static str = env!("CARGO_CRATE_NAME");
static INIT_LOGGING: Once = Once::new();

const fn default_log_config() -> LogConfig<'static> {
    LogConfig::new(Some(CRATE_NAME), DEFAULT_LOG_LEVEL)
}

fn init_env_logger<F, S>(key: F, style_key: S)
where
    F: Into<Cow<'static, str>> + AsRef<str>,
    S: Into<Cow<'static, str>> + AsRef<str>,
{
    // let log_env = |k: &str| {
    //     let v = env::var(k)
    //         .ok()
    //         .map(Cow::Owned)
    //         .unwrap_or(Cow::Borrowed(""));
    //     eprintln!("{}={}", k, v);
    // };
    // log_env(key.as_ref());
    // log_env(style_key.as_ref());
    let e = env_logger::Env::new()
        .filter(key)
        .write_style(style_key);
    env_logger::init_from_env(e);
}

pub fn init() {
    use std::convert::TryFrom;
    INIT_LOGGING.call_once(|| {
        let v = env::var("RUST_LOG").ok();
        let mut cfg = LogConfigList::try_from(v.as_ref().map(AsRef::as_ref))
            .unwrap_or_else(|e| {
                eprintln!("Failed parsing log config: {}", &e);
                LogConfigList::default()
            });
        if !cfg.has_config(CRATE_NAME) {
            let default_config = default_log_config();
            cfg.insert_config(default_config);
            cfg.store("RUST_LOG");
        }
        init_env_logger("RUST_LOG", "RUST_LOG_STYLE");
        // env_or_default("RUST_LOG", format!("{}={}", env!("CARGO_CRATE_NAME"), DEFAULT_LOG_LEVEL));
        // env_logger::init();
    });
}
