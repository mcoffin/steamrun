use std::{
    env,
    path::PathBuf,
};

pub fn config_dir() -> PathBuf {
    dirs::config_dir().unwrap_or_else(|| {
        let mut ret: PathBuf = env::var("HOME").map(PathBuf::from).unwrap();
        ret.extend(&[".config", "steamrun"]);
        ret
    })
}

lazy_static! {
    static ref BASE_CONFIG_DIR: PathBuf = {
        dirs::config_dir().unwrap_or_else(|| {
            let mut ret: PathBuf = env::var("HOME").map(PathBuf::from).unwrap();
            ret.push(".config");
            ret
        })
    };
    pub static ref CONFIG_DIR: PathBuf = {
        dirs::config_dir().unwrap_or_else(|| {
            let mut ret: PathBuf = BASE_CONFIG_DIR.clone();
            ret.push("steamrun");
            ret
        })
    };
    pub static ref GLOBAL_CONFIG_FILE: PathBuf = {
        let mut p = BASE_CONFIG_DIR.clone();
        p.push("steamrun.yml");
        debug!("GLOBAL_CONFIG_FILE: {}", p.display());
        p
    };
}
