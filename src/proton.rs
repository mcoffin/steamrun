use crate::ApplicableConfig;

use mcoffin_option_ext::OptionExtDefault;
use std::{
    env,
    ffi::OsStr,
    io,
    path::PathBuf,
};
use serde::{
    Deserialize,
};
use crate::env_ext::EnvSetting;

#[derive(Debug, Deserialize, Clone)]
#[serde(untagged)]
pub enum ProtonLogValue {
    Enabled(bool),
    Level(u8),
    Extended(String),
}

impl Default for ProtonLogValue {
    #[inline(always)]
    fn default() -> Self {
        ProtonLogValue::Enabled(false)
    }
}

const TRUTHY_STRING: &'static str = "1";

impl ProtonLogValue {
    fn value<'a>(&'a self) -> EnvSetting<&'a str> {
        use ProtonLogValue::*;
        match self {
            &Enabled(false) | &Level(0) => EnvSetting::Unset,
            &Enabled(true) | &Level(_) => EnvSetting::Set(TRUTHY_STRING),
            &Extended(ref s) => EnvSetting::Set(s.as_ref()),
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct ProtonConfig {
    pub fsync: Option<bool>,
    pub esync: Option<bool>,
    dump_debug_commands: Option<bool>,
    log: Option<ProtonLogValue>,
    log_dir: Option<PathBuf>,
}

impl ProtonConfig {
    #[inline(always)]
    pub fn fsync(&self) -> bool {
        self.fsync.unwrap_or(true)
    }

    #[inline(always)]
    pub fn esync(&self) -> bool {
        self.esync.unwrap_or(true)
    }

    #[inline(always)]
    pub fn dump_debug_commands(&self) -> bool {
        self.dump_debug_commands.unwrap_or(false)
    }
}

#[inline(always)]
pub(crate) fn handle_negative_bool<K, V>(b: Option<bool>, key: K, value: V) where
    K: AsRef<OsStr>,
    V: AsRef<OsStr>,
{
    EnvSetting::new(b, value, true).apply(key);
}

#[inline(always)]
pub(crate) fn handle_positive_bool<K, V>(b: Option<bool>, key: K, value: V) where
    K: AsRef<OsStr>,
    V: AsRef<OsStr>,
{
    EnvSetting::new(b, value, false).apply(key);
}

impl ApplicableConfig for ProtonConfig {
    fn steamrun_apply(&self) -> io::Result<()> {
        handle_negative_bool(self.fsync, "PROTON_NO_FSYNC", TRUTHY_STRING);
        handle_negative_bool(self.esync, "PROTON_NO_ESYNC", TRUTHY_STRING);
        let log_value = self.log.as_ref()
            .map(ProtonLogValue::value)
            .or_default();
        trace!("proton.log: {:?}", &log_value);
        log_value.apply("PROTON_LOG");
        handle_positive_bool(self.dump_debug_commands, "PROTON_DUMP_DEBUG_COMMANDS", TRUTHY_STRING);
        if let Some(log_dir) = self.log_dir.as_ref() {
            env::set_var("PROTON_LOG_DIR", log_dir);
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TRUTHY_VAL: &'static str = "1";

    #[test]
    fn handle_positive_bool_works() {
        const KEY: &'static str = "STEAMRUN_TEST_HANDLE_POSITIVE";

        env::set_var(KEY, TRUTHY_VAL);
        handle_positive_bool(None, KEY, TRUTHY_VAL);
        assert_eq!(env::var(KEY).ok().as_ref().map(AsRef::as_ref), Some("1"));

        env::remove_var(KEY);
        handle_positive_bool(None, KEY, TRUTHY_VAL);
        assert_eq!(env::var(KEY).ok(), None);

        env::remove_var(KEY);
        handle_positive_bool(Some(true), KEY, TRUTHY_VAL);
        assert_eq!(env::var(KEY).ok().as_ref().map(AsRef::as_ref), Some("1"));

        env::set_var(KEY, TRUTHY_VAL);
        handle_positive_bool(Some(false), KEY, TRUTHY_VAL);
        assert_eq!(env::var(KEY).ok(), None);
    }

    #[test]
    fn handle_negative_bool_works() {
        const KEY: &'static str = "STEAMRUN_TEST_HANDLE_NEGATIVE";

        env::set_var(KEY, TRUTHY_VAL);
        handle_negative_bool(None, KEY, TRUTHY_VAL);
        assert_eq!(env::var(KEY).ok().as_ref().map(AsRef::as_ref), Some("1"));

        env::remove_var(KEY);
        handle_negative_bool(None, KEY, TRUTHY_VAL);
        assert!(env::var_os(KEY).is_none());

        env::remove_var(KEY);
        assert!(env::var_os(KEY).is_none());
        handle_negative_bool(Some(true), KEY, TRUTHY_VAL);
        assert!(env::var_os(KEY).is_none());

        env::set_var(KEY, TRUTHY_VAL);
        handle_negative_bool(Some(false), KEY, TRUTHY_VAL);
        assert_eq!(env::var(KEY).ok().as_ref().map(AsRef::as_ref), Some("1"));
    }
}
