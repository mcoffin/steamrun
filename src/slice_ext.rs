#[inline(always)]
pub fn slice_iter<'a, T: ?Sized>(a: &'a [&'a T]) -> impl Iterator<Item=&'a T> {
    a.iter().map(|&v| v)
}
