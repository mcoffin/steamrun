use std::{
    fs,
    io::{
        self,
        ErrorKind,
    },
    path::Path,
};

pub fn ensure_dir<P: AsRef<Path>>(path: P) -> io::Result<()> {
    let path = path.as_ref();
    let metadata = fs::metadata(path)
        .or_else(|_| fs::create_dir_all(path).and_then(|_| fs::metadata(path)))?;
    if metadata.is_dir() {
        Ok(())
    } else {
        let msg = format!("{} already exists and is not a directory!", path.display());
        Err(io::Error::new(ErrorKind::AlreadyExists, msg))
    }
}
