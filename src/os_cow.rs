use std::{
    borrow::Cow,
    ffi::OsStr,
};

#[repr(transparent)]
pub struct OsCow<'a>(Cow<'a, str>);

impl<'a> AsRef<str> for OsCow<'a> {
    #[inline(always)]
    fn as_ref(&self) -> &str {
        self.0.as_ref()
    }
}

impl<'a> AsRef<OsStr> for OsCow<'a> {
    #[inline(always)]
    fn as_ref(&self) -> &OsStr {
        self.0.as_ref().as_ref()
    }
}

impl<'a> From<Cow<'a, str>> for OsCow<'a> {
    #[inline(always)]
    fn from(inner: Cow<'a, str>) -> OsCow<'a> {
        OsCow(inner)
    }
}

impl<'a> Into<Cow<'a, str>> for OsCow<'a> {
    #[inline(always)]
    fn into(self) -> Cow<'a, str> {
        self.0
    }
}
