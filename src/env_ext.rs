use std::{
    env,
    ffi::{
        OsStr,
        OsString,
    },
    fmt,
    iter,
};

pub enum EnvSetting<S: AsRef<OsStr>> {
    Set(S),
    Unset,
    Ignore,
}

impl<S: AsRef<OsStr>> fmt::Debug for EnvSetting<S> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use EnvSetting::*;
        match self {
            &Set(ref s) => {
                let s: &OsStr = s.as_ref();
                f.debug_tuple("Set")
                    .field(&Box::new(s))
                    .finish()
            },
            &Unset => write!(f, "Unset"),
            &Ignore => write!(f, "Ignore"),
        }
    }
}

impl EnvSetting<&'static str> {
    pub const fn from_setting(value: Option<bool>, set_value: &'static str, negative: bool) -> Self {
        match value {
            Some(b) if b != negative => EnvSetting::Set(set_value),
            Some(_) => EnvSetting::Unset,
            None => EnvSetting::Ignore,
        }
    }
}

impl<S: AsRef<OsStr>> EnvSetting<S> {
    #[inline]
    pub fn new(value: Option<bool>, set_value: S, negative: bool) -> Self {
        value
            .map(move |b| if !b && negative || b && !negative {
                EnvSetting::Set(set_value)
            } else {
                EnvSetting::Unset
            })
            .unwrap_or(EnvSetting::Ignore)
    }

    pub fn apply<K: AsRef<OsStr>>(&self, key: K) {
        use EnvSetting::*;
        match self {
            &Set(ref v) => env::set_var(key, v.as_ref()),
            Unset => env::remove_var(key),
            Ignore => {}
        }
    }
}

impl<S: AsRef<OsStr>> Default for EnvSetting<S> {
    #[inline(always)]
    fn default() -> Self {
        EnvSetting::Ignore
    }
}

pub fn non_empty_var_os<K: AsRef<OsStr>>(key: K) -> Option<OsString> {
    env::var_os(key)
        .filter(|v| v.len() > 0)
}

pub fn non_empty_var<K: AsRef<OsStr>>(key: K) -> Option<String> {
    env::var(key)
        .ok()
        .filter(|v| v.len() > 0)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ExtendPosition {
    Before,
    After,
}

pub const DEFAULT_SEPARATOR: &'static str = ":";

pub fn extend_env<K, It>(key: K, values: It, position: ExtendPosition, separator: Option<&str>) where
    K: AsRef<OsStr>,
    It: IntoIterator,
    <It as IntoIterator>::Item: AsRef<str>,
{
    use std::collections::LinkedList;
    let separator = separator.unwrap_or(DEFAULT_SEPARATOR);
    let key = key.as_ref();
    let values: LinkedList<_> = values.into_iter().collect();
    let values = values.iter().map(AsRef::as_ref);
    let old_value = non_empty_var(key);
    let new_values: Box<dyn Iterator<Item=&str>> = if let Some(v) = old_value.as_ref() {
        let vs = v.split(separator);
        match position {
            ExtendPosition::After => Box::new(vs.chain(values)),
            ExtendPosition::Before => Box::new(values.chain(vs)),
        }
    } else {
        Box::new(values.into_iter())
    };
    let new_value = new_values.fold(None, |s: Option<String>, v: &str| {
        let ret = s
            .map(|mut s| {
                use std::fmt::Write;
                let _result = write!(&mut s, "{}{}", separator, v);
                s
            })
            .unwrap_or_else(|| v.to_string());
        Some(ret)
    });
    if let Some(v) = new_value {
        env::set_var(key, v);
    }
}

pub struct ListDescriptor<'s, K: AsRef<OsStr>> {
    pub key: K,
    pub separator: &'s str,
}

impl<'s, K> ListDescriptor<'s, K> where
    K: AsRef<OsStr>,
{
    pub fn from_key(key: K) -> Self {
        ListDescriptor {
            key: key,
            separator: "",
        }
    }

    pub fn from_key_and_separator(key: K, separator: &'s str) -> Self {
        ListDescriptor {
            key: key,
            separator: separator,
        }
    }

    pub fn new(key: K, separator: Option<&'s str>) -> Self {
        ListDescriptor::from_key_and_separator(key, separator.unwrap_or(DEFAULT_SEPARATOR))
    }

    #[inline(always)]
    pub fn key(&self) -> &OsStr {
        self.key.as_ref()
    }

    pub fn get<'a>(&'a self) -> impl Iterator<Item=String> + 'a {
        env_list(self.key(), Some(self.separator))
    }

    pub fn contains(&self, v: &str) -> bool {
        self.get()
            .find(|s| s == v)
            .is_some()
    }

    pub fn extend_if_not_contains<It>(&self, it: It, position: ExtendPosition) where
        It: IntoIterator,
        <It as IntoIterator>::Item: AsRef<str>,
    {
        let values = it
            .into_iter()
            .filter(|s| !self.contains(s.as_ref()));
        extend_env(self.key(), values, position, Some(self.separator));
    }

    #[inline]
    pub fn prepend_if_not_contains<It>(&self, it: It) where
        It: IntoIterator,
        <It as IntoIterator>::Item: AsRef<str>,
    {
        self.extend_if_not_contains(it, ExtendPosition::Before);
    }

    #[inline]
    pub fn append_if_not_contains<It>(&self, it: It) where
        It: IntoIterator,
        <It as IntoIterator>::Item: AsRef<str>,
    {
        self.extend_if_not_contains(it, ExtendPosition::After);
    }
}

impl<'s, K: AsRef<OsStr>> From<(K, &'s str)> for ListDescriptor<'s, K> {
    #[inline]
    fn from(tup: (K, &'s str)) -> Self {
        let (key, sep) = tup;
        ListDescriptor::from_key_and_separator(key, sep)
    }
}

pub fn env_list<'a, K: AsRef<OsStr>>(key: K, separator: Option<&'a str>) -> impl Iterator<Item=String> + 'a {
    use std::{
        collections::LinkedList,
    };
    let separator: &'a str = separator.unwrap_or(DEFAULT_SEPARATOR);
    env::var(key)
        .ok()
        .into_iter()
        .flat_map(move |v| v.split(separator).map(ToOwned::to_owned).collect::<LinkedList<_>>())
}

#[inline]
pub fn add_env<K: AsRef<OsStr>, V: AsRef<str>>(key: K, value: V, position: ExtendPosition, separator: Option<&str>) {
    extend_env(key, iter::once(value), position, separator)
}

#[inline]
pub fn append_env<K, It>(key: K, values: It, separator: Option<&str>) where
    K: AsRef<OsStr>,
    It: IntoIterator,
    <It as IntoIterator>::Item: AsRef<str>,
{
    extend_env(key, values, ExtendPosition::After, separator)
}

#[inline]
pub fn prepend_env<K, It>(key: K, values: It, separator: Option<&str>) where
    K: AsRef<OsStr>,
    It: IntoIterator,
    <It as IntoIterator>::Item: AsRef<str>,
{
    extend_env(key, values, ExtendPosition::Before, separator)
}

#[cfg(test)]
mod tests {
    use std::{
        env,
        ffi::OsStr,
        iter,
    };
    use super::*;
    use crate::slice_ext::slice_iter;

    const TEST_VAR_BLANK: &'static str = "STEAMRUN_TEST_VAR_BLANK";

    fn test_var(name: &str) -> String {
        format!("STEAMRUN_TEST_VAR_{}", name)
    }

    #[inline(always)]
    fn assert_env_eq<K: AsRef<OsStr>, V: AsRef<str>>(key: K, value: Option<V>) {
        let value = value.as_ref().map(AsRef::as_ref);
        let real_value = env::var(key).ok();
        assert_eq!(value, real_value.as_ref().map(AsRef::as_ref));
    }

    #[test]
    fn extend_env_works_with_blank() {
        let old_value = env::var_os(TEST_VAR_BLANK);
        if old_value.is_some() {
            env::remove_var(TEST_VAR_BLANK);
        }
        append_env(TEST_VAR_BLANK, iter::once("foo"), None);
        assert_env_eq(TEST_VAR_BLANK, Some("foo"));
        env::remove_var(TEST_VAR_BLANK);
        prepend_env(TEST_VAR_BLANK, iter::once("foo"), None);
        assert_env_eq(TEST_VAR_BLANK, Some("foo"));
        old_value.into_iter().for_each(|v| {
            env::set_var(TEST_VAR_BLANK, v);
        });
    }

    #[test]
    fn extend_env_append_prepend() {
        let k = test_var("APPEND_PREPEND");
        let old_value = env::var_os(&k);
        if old_value.is_some() {
            env::remove_var(&k);
        }
        env::set_var(&k, "foo");
        append_env(&k, slice_iter(&["bar", "baz"]), None);
        assert_env_eq(&k, Some("foo:bar:baz"));

        env::set_var(&k, "foo:bar");
        append_env(&k, iter::once("baz"), None);
        assert_env_eq(&k, Some("foo:bar:baz"));

        env::set_var(&k, "foo:bar");
        append_env(&k, iter::once("baz").chain(iter::once("bazoo")), None);
        assert_env_eq(&k, Some("foo:bar:baz:bazoo"));

        env::set_var(&k, "foo");
        prepend_env(&k, slice_iter(&["bar", "baz"]), None);
        assert_env_eq(&k, Some("bar:baz:foo"));

        env::set_var(&k, "foo");
        append_env(&k, iter::once("bar"), None);
        prepend_env(&k, iter::once("baz"), None);
        assert_env_eq(&k, Some("baz:foo:bar"));
    }
    
    #[test]
    fn extend_if_not_contains_filters() {
        let k = test_var("APPEND_IF_NOT_CONTAINS");
        let old_value = env::var_os(&k);
        if old_value.is_some() {
            env::remove_var(&k);
        }

        let descriptor = ListDescriptor::new(&k, Some(":"));
        env::set_var(&k, "foo:bar");
        descriptor
            .prepend_if_not_contains(slice_iter(&["bar", "baz"]));
        assert_env_eq(&k, Some("baz:foo:bar"));
        descriptor
            .append_if_not_contains(slice_iter(&["foo", "bazoo"]));
        assert_env_eq(&k, Some("baz:foo:bar:bazoo"));

        if let Some(old_value) = old_value {
            env::set_var(&k, old_value);
        }
    }

    #[test]
    fn positive_env_setting_works() {
        const KEY: &'static str = "STEAMRUN_TEST_POSITIVE_ENV_SETTING";
        const TRUTHY_VAL: &'static str = "true";

        env::set_var(KEY, "foobar");
        EnvSetting::from_setting(None, TRUTHY_VAL, false).apply(KEY);
        assert_eq!(env::var(KEY).ok().as_ref().map(AsRef::as_ref), Some("foobar"));

        env::remove_var(KEY);
        EnvSetting::from_setting(None, TRUTHY_VAL, false).apply(KEY);
        assert!(env::var_os(KEY).is_none());

        EnvSetting::from_setting(Some(true), TRUTHY_VAL, false).apply(KEY);
        assert_eq!(env::var(KEY).ok().as_ref().map(AsRef::as_ref), Some(TRUTHY_VAL));

        EnvSetting::from_setting(Some(false), TRUTHY_VAL, false).apply(KEY);
        assert!(env::var_os(KEY).is_none());
    }

    #[test]
    fn negative_env_setting_works() {
        const KEY: &'static str = "STEAMRUN_TEST_POSITIVE_ENV_SETTING";
        const TRUTHY_VAL: &'static str = "true";

        env::set_var(KEY, "foobar");
        EnvSetting::from_setting(None, TRUTHY_VAL, false).apply(KEY);
        assert_eq!(env::var(KEY).ok().as_ref().map(AsRef::as_ref), Some("foobar"));

        env::remove_var(KEY);
        EnvSetting::from_setting(None, TRUTHY_VAL, false).apply(KEY);
        assert!(env::var_os(KEY).is_none());

        EnvSetting::from_setting(Some(false), TRUTHY_VAL, false).apply(KEY);
        assert!(env::var_os(KEY).is_none());

        EnvSetting::from_setting(Some(true), TRUTHY_VAL, false).apply(KEY);
        assert_eq!(env::var(KEY).ok().as_ref().map(AsRef::as_ref), Some(TRUTHY_VAL));
    }
}
