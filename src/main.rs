#[macro_use] extern crate log;
extern crate env_logger;
extern crate clap;
extern crate serde;
extern crate serde_yaml;
extern crate dirs;
extern crate thiserror;
#[macro_use] extern crate lazy_static;
extern crate mcoffin_option_ext;
extern crate env_logger_config;
extern crate regex;

mod logging;
pub mod dirs_ext;
pub mod env_ext;
pub mod slice_ext;
pub mod fs_ext;
pub mod util;
pub mod env_operations;
pub mod cmd_operations;
mod os_cow;
mod proton;
mod dxvk;
mod mesa;
mod side_effects;

use clap::Parser;

use std::{
    borrow::Cow,
    collections::{
        HashMap,
    },
    convert::TryFrom,
    error::Error,
    env,
    fmt::{
        self,
        Display,
    },
    fs,
    io,
    iter,
    path::{
        Path,
        PathBuf,
    },
    process::{
        Command,
        ExitStatus,
        Stdio,
    },
    str::FromStr,
    thread,
};
use serde::{
    Deserialize,
};
pub use cmd_operations::CmdOperation;
use os_cow::OsCow;
pub use proton::ProtonConfig;
pub use dxvk::DxvkConfig;
pub use mesa::MesaConfig;
pub use env_operations::EnvOperation;
use side_effects::{
    configure,
    side_effect,
};

pub(crate) trait ApplicableConfig {
    fn steamrun_apply(&self) -> io::Result<()>;
}

#[derive(Debug, thiserror::Error)]
pub enum ConfigError {
    #[error("{0}")]
    Io(#[from] io::Error),
    #[error("Error parsing config YAML: {0}")]
    Yaml(#[from] serde_yaml::Error),
}

#[derive(Debug, Deserialize, Clone)]
pub struct SteamrunConfig {
    env: Option<HashMap<String, String>>,
    env_remove: Option<Vec<String>>,
    env_operations: Option<Vec<EnvOperation>>,
    #[serde(default = "SteamrunConfig::default_cmd_operations")]
    cmd_operations: Vec<CmdOperation>,
    vulkan_device_filter: Option<String>,
    dxvk: Option<DxvkConfig>,
    proton: Option<ProtonConfig>,
    mesa: Option<MesaConfig>,
    log_directory: Option<PathBuf>,
    #[serde(default = "ExecMethod::exec")]
    exec_method: ExecMethod,
    default_modes: Option<Vec<String>>,
    #[serde(default = "SteamrunConfig::serde_default_modes")]
    modes: Option<HashMap<String, SteamrunConfig>>,
}

impl Default for SteamrunConfig {
    #[inline(always)]
    fn default() -> Self {
        SteamrunConfig {
            env: None,
            env_remove: None,
            env_operations: None,
            cmd_operations: Vec::new(),
            vulkan_device_filter: None,
            dxvk: None,
            proton: None,
            mesa: None,
            log_directory: None,
            exec_method: ExecMethod::default(),
            default_modes: None,
            // This has to be present, because somewhere I optimized cases where it isn't present,
            // and it'll skip checking the global config for modes
            modes: Some(HashMap::new()),
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct SteamrunGlobalConfig {
    modes: Option<HashMap<String, SteamrunConfig>>,
}

lazy_static! {
    static ref EMPTY_MODES: HashMap<String, SteamrunConfig> = HashMap::new();
}

impl SteamrunConfig {
    pub fn default_cmd_operations() -> Vec<CmdOperation> {
        Vec::new()
    }

    pub fn serde_default_modes() -> Option<HashMap<String, SteamrunConfig>> {
        Some(HashMap::new())
    }

    #[inline(always)]
    pub fn mesa<'a>(&'a self) -> Cow<'a, MesaConfig> {
        self.mesa.as_ref()
            .map(Cow::Borrowed)
            .unwrap_or_else(|| Cow::Owned(MesaConfig::default()))
    }

    pub fn env(&self) -> &HashMap<String, String> {
        lazy_static! {
            static ref EMPTY_ENV: HashMap<String, String> = HashMap::new();
        }
        self.env.as_ref()
            .unwrap_or(&EMPTY_ENV)
    }

    pub fn env_operations<'a>(&'a self) -> impl Iterator<Item=&'a EnvOperation> {
        lazy_static! {
            static ref EMPTY_OPERATIONS: Vec<EnvOperation> = Vec::new();
        }
        self.env_operations.as_ref()
            .unwrap_or(&EMPTY_OPERATIONS)
            .iter()
    }

    #[inline(always)]
    pub fn vulkan_device_filter<'a>(&'a self) -> Option<&'a str> {
        self.vulkan_device_filter.as_ref().map(AsRef::as_ref)
    }

    #[inline(always)]
    pub fn log_directory<'a>(&'a self) -> Option<&'a Path> {
        self.log_directory.as_ref().map(AsRef::as_ref)
    }

    pub fn log_path(&self, name: &str, log_type: &str) -> Option<PathBuf> {
        self.log_directory()
            .map(PathBuf::from)
            .map(configure(|p: &mut PathBuf| {
                let filename = format!("{}.{}.log", name, log_type);
                p.push(filename);
            }))
    }

    pub fn env_remove<'a>(&'a self) -> impl Iterator<Item=&'a str> {
        let maybe_it = self.env_remove.as_ref()
            .map(|v| v.iter().map(AsRef::as_ref));
        util::MaybeIterator::from(maybe_it)
    }

    pub fn modes(&self) -> &HashMap<String, SteamrunConfig> {
        if let Some(modes) = self.modes.as_ref() {
            modes
        } else {
            &*EMPTY_MODES
        }
    }

    pub fn default_modes<'a>(&'a self) -> impl Iterator<Item=&'a str> {
        lazy_static! {
            static ref EMPTY_DEFAULT_MODES: Vec<String> = Vec::new();
        }
        let default_modes = if let Some(modes) = self.default_modes.as_ref() {
            modes
        } else {
            &*EMPTY_DEFAULT_MODES
        };
        default_modes.iter().map(AsRef::as_ref)
    }

    pub fn cmd_operations<It>(&self, modes: It) -> Vec<&CmdOperation>
    where
        It: IntoIterator,
        <It as IntoIterator>::Item: AsRef<str>,
    {
        let mut ops: Vec<_> = self.cmd_operations.iter()
            .collect();
        modes.into_iter()
            .filter_map(|m| self.modes().get(m.as_ref()))
            .for_each(|mode| ops.extend(mode.cmd_operations.iter()));
        ops
    }
}

impl SteamrunGlobalConfig {
    #[inline(always)]
    pub fn path() -> &'static Path {
        let p: &'static PathBuf = &dirs_ext::GLOBAL_CONFIG_FILE;
        p.as_ref()
    }

    pub fn modes(&self) -> &HashMap<String, SteamrunConfig> {
        self.modes.as_ref().unwrap_or(&*EMPTY_MODES)
    }
}

impl Default for SteamrunGlobalConfig {
    fn default() -> Self {
        SteamrunGlobalConfig {
            modes: None,
        }
    }
}

impl ApplicableConfig for SteamrunConfig {
    fn steamrun_apply(&self) -> io::Result<()> {
        self.env_remove().for_each(|k| {
            env::remove_var(k);
        });
        self.env().iter().for_each(|(k, v)| {
            env::set_var(k, v);
        });
        self.env_operations().try_for_each(ApplicableConfig::steamrun_apply)?;
        if let Some(dxvk) = self.dxvk.as_ref() {
            dxvk.steamrun_apply()?;
        }
        if let Some(proton) = self.proton.as_ref() {
            proton.steamrun_apply()?;
        }
        self.mesa().steamrun_apply()?;
        if let Some(filter) = self.vulkan_device_filter().filter(|s| s.len() > 0) {
            set_rust_ld()?;
            env_ext::ListDescriptor::new("VK_INSTANCE_LAYERS", Some(":"))
                .prepend_if_not_contains(["VK_LAYER_MCOF_device_filter", "VK_LAYER_MCOF_device_filter_32"].iter().map(|&v| v));
            env::set_var("VK_DEVICE_FILTER", filter);
        }
        if let Some(log_dir) = self.log_directory() {
            fs_ext::ensure_dir(log_dir)?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum ExecMethodParseError<'a> {
    #[error("Unknown exec method: {0}")]
    UnknownExecMethod(Cow<'a, str>),
}

impl<'a> ExecMethodParseError<'a> {
    pub fn into_owned(self) -> ExecMethodParseError<'static> {
        use ExecMethodParseError::*;
        match self {
            UnknownExecMethod(s) => ExecMethodParseError::from(s.into_owned()),
        }
    }
}

impl<'a, T> From<T> for ExecMethodParseError<'a> where
    T: Into<Cow<'a, str>>,
{
    #[inline(always)]
    fn from(s: T) -> Self {
        use ExecMethodParseError::*;
        UnknownExecMethod(s.into())
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum ExecMethod {
    Exec,
    SubProcess,
}

impl Display for ExecMethod {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

impl ExecMethod {
    pub const fn exec() -> Self {
        ExecMethod::Exec
    }
    pub fn as_str(&self) -> &'static str {
        match *self {
            ExecMethod::Exec => "exec",
            ExecMethod::SubProcess => "subprocess",
        }
    }
}

impl Default for ExecMethod {
    #[inline(always)]
    fn default() -> Self {
        ExecMethod::Exec
    }
}

impl<'a> TryFrom<&'a str> for ExecMethod {
    type Error = ExecMethodParseError<'a>;
    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match s {
            "exec" => Ok(ExecMethod::Exec),
            "subprocess" => Ok(ExecMethod::SubProcess),
            _ => Err(ExecMethodParseError::from(s))
        }
    }
}

impl FromStr for ExecMethod {
    type Err = ExecMethodParseError<'static>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::try_from(s)
            .map_err(ExecMethodParseError::into_owned)
    }
}

#[derive(Debug, thiserror::Error)]
pub enum CommandError {
    #[error("Error while running command: {0}")]
    Io(#[from] io::Error),
    #[error("Command exited with failure status")]
    ProcessFailure,
    #[error("Unimplemented exec method: {0}")]
    UnimplementedExecMethod(ExecMethod),
}

#[derive(Debug, Parser)]
#[clap(author = clap::crate_authors!(), version = clap::crate_version!(), about = clap::crate_description!())]
struct Config {
    #[clap(long, short, required = true)]
    name: String,
    #[clap(long, parse(try_from_str))]
    exec_method: Option<ExecMethod>,
    #[clap(long, short)]
    verbose: bool,
    command: Vec<String>,
    #[clap(long, short = 'd')]
    config_path: Option<PathBuf>,
    #[clap(short, long = "mode")]
    modes: Vec<String>,
    #[clap(long)]
    no_default_modes: bool,
}

fn dump_logs<R, W>(mut r: R, mut w: W) -> thread::JoinHandle<io::Result<u64>> where
    R: io::Read + Send + 'static,
    W: io::Write + Send + 'static,
{
    thread::spawn(move || {
        io::copy(&mut r, &mut w)
    })
}

impl Config {
    #[inline(always)]
    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    #[inline(always)]
    pub fn is_verbose(&self) -> bool {
        self.verbose
    }

    #[inline(always)]
    pub fn use_default_modes(&self) -> bool {
        !self.no_default_modes
    }
    
    pub fn modes<'a, It>(&'a self, default_modes: It) -> Vec<&'a str>
    where
        It: IntoIterator<Item=&'a str>,
    {
        if self.use_default_modes() {
            default_modes.into_iter()
                .chain(self.modes.iter().map(AsRef::as_ref))
                .collect()
        } else {
            self.modes.iter()
                .map(AsRef::as_ref)
                .collect()
        }
    }

    pub fn config_base<'a>(&'a self) -> Cow<'a, Path> {
        self.config_path.as_ref()
            .map(|p| Cow::Borrowed(p.as_ref()))
            .unwrap_or_else(|| Cow::Owned(dirs_ext::CONFIG_DIR.clone()))
    }

    pub fn config_path(&self) -> PathBuf {
        let mut ret: PathBuf = self.config_base().into_owned();
        ret.push("steamrun");
        ret.push(format!("{}.yml", self.name()));
        ret
    }

    pub fn config(&self) -> Result<SteamrunConfig, ConfigError> {
        let cfg_path = self.config_path();
        debug!("app config path: {}", cfg_path.display());
        let mut ret: SteamrunConfig = if cfg_path.exists() {
            let f = fs::OpenOptions::new()
                .read(true)
                .open(cfg_path)
                .map_err(ConfigError::from)?;
            serde_yaml::from_reader(f)
                .map_err(ConfigError::from)?
        } else {
            SteamrunConfig::default()
        };
        debug!("app config: {:?}", &ret);
        debug!("global config path: {}", SteamrunGlobalConfig::path().display());
        let global_config: SteamrunGlobalConfig = fs::OpenOptions::new()
            .read(true)
            .open(SteamrunGlobalConfig::path())
            .map_err(ConfigError::from)
            .and_then(|f| serde_yaml::from_reader(f).map_err(ConfigError::from))?;
        debug!("global config: {:?}", &global_config);
        if let Some(global_modes) = global_config.modes.as_ref() {
            if let Some(modes) = ret.modes.as_mut() {
                modes.extend(global_modes.iter().map(|(k, v)| (k.clone(), v.clone())));
            }
        }
        Ok(ret)
    }

    pub fn command_strings<'o, It>(&self, operations: It) -> Result<Vec<Cow<'_, str>>, Box<dyn Error>>
    where
        It: IntoIterator<Item=&'o CmdOperation>,
    {
        let cmd = self.command.iter()
            .map(AsRef::as_ref)
            .map(Cow::Borrowed)
            .collect();
        operations.into_iter()
            .fold(Ok(cmd), |cmd, op| cmd.and_then(|cmd| op.apply(cmd)))
    }

    pub fn command<'o, It>(&self, operations: It) -> Option<Command>
    where
        It: IntoIterator<Item=&'o CmdOperation>,
    {
        let mut cmd = self.command_strings(operations)
            .map_err(side_effect(|e| error!("Error running command operations: {}", &e)))
            .ok()?
            .into_iter()
            .map(OsCow::from);
        cmd.next()
            .map(Command::new)
            .map(configure(move |command: &mut Command| {
                command.args(cmd);
            }))
    }

    fn log_path<D: Into<PathBuf>>(&self, directory: D, log_type: &str) -> PathBuf {
        let mut ret = directory.into();
        let filename = format!("{}.command-{}.log", self.name(), log_type);
        ret.push(filename);
        ret
    }

    fn open_logs<P: AsRef<Path>>(&self, directory: P) -> io::Result<(fs::File, fs::File)> {
        let directory = directory.as_ref();
        fs_ext::ensure_dir(directory)?;
        let open_log_file = |s: &str| {
            fs::OpenOptions::new()
                .write(true)
                .create(true)
                .open(self.log_path(directory, s))
        };
        let stdout_file = open_log_file("stdout")?;
        let stderr_file = open_log_file("stderr")?;
        Ok((stdout_file, stderr_file))
    }

    pub fn run_command<'o, P, It>(&self, method: ExecMethod, log_directory: Option<P>, cmd_operations: It) -> Result<(), CommandError> where
        P: AsRef<Path>,
        It: IntoIterator<Item=&'o CmdOperation>,
    {
        let method = self.exec_method.unwrap_or(method);
        match method {
            ExecMethod::SubProcess => {
                if let Some(mut command) = self.command(cmd_operations) {
                    if let Some(log_directory) = log_directory {
                        let (stdout_file, stderr_file) = self.open_logs(log_directory)
                            .map_err(CommandError::from)?;
                        command
                            .stdin(Stdio::inherit())
                            .stdout(Stdio::piped())
                            .stderr(Stdio::piped());
                        let mut child = command.spawn()
                            .map_err(CommandError::from)?;
                        let stdout = child.stdout.take().unwrap();
                        let stderr = child.stderr.take().unwrap();
                        let stdout_thread = dump_logs(stdout, stdout_file);
                        let stderr_thread = dump_logs(stderr, stderr_file);
                        child.wait()
                            .map_err(CommandError::from)
                            .and_then(ExitStatusExt::check_status)?;
                        iter::once(stdout_thread).chain(iter::once(stderr_thread)).for_each(|handle| {
                            if let Err(e) = handle.join() {
                                error!("Error in log writing thread: {:?}", &e);
                            } 
                        });
                    } else {
                        command.status()
                            .map_err(CommandError::from)
                            .and_then(ExitStatusExt::check_status)?;
                    }
                    debug!("command successfully completed");
                    Ok(())
                } else {
                    warn!("no command supplied");
                    Ok(())
                }
            },
            _ => {
                use std::os::unix::process::CommandExt;
                if let Some(mut command) = self.command(cmd_operations) {
                    return Err(CommandError::from(command.exec()));
                } else {
                    warn!("no command supplied");
                    Ok(())
                }
            },
        }
    }
}

const RUST_RUNTIME_TARGETS: &'static [&'static str] = &[
    "i686-unknown-linux-gnu",
    "x86_64-unknown-linux-gnu"
];

fn set_rust_ld() -> io::Result<()> {
    let get_ld_library_path = || env::var("LD_LIBRARY_PATH").map(Cow::Owned).unwrap_or(Cow::Borrowed(""));
    debug!("set_rust_ld: before LD_LIBRARY_PATH={}", get_ld_library_path());
    let rustlib_base = {
        let mut p = PathBuf::from(env::var("HOME").unwrap());
        p.push(".rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib");
        p
    };
    let lib_dirs = slice_ext::slice_iter(RUST_RUNTIME_TARGETS)
        .map(|target| {
            let mut p = rustlib_base.clone();
            p.push(target);
            p.push("lib");
            p
        })
        .map(|p| p.to_string_lossy().into_owned());
    env_ext::ListDescriptor::new("LD_LIBRARY_PATH", Some(":"))
        .prepend_if_not_contains(lib_dirs);
    debug!("set_rust_ld: after LD_LIBRARY_PATH={}", get_ld_library_path());
    Ok(())
}

trait ExitStatusExt: Sized {
    fn check_status(self) -> Result<Self, CommandError>;
}

impl ExitStatusExt for ExitStatus {
    fn check_status(self) -> Result<Self, CommandError> {
        if self.success() {
            Ok(self)
        } else {
            Err(CommandError::ProcessFailure)
        }
    }
}

trait ConfigExt {
    fn run_config(&self, steamrun_config: &SteamrunConfig) -> Result<(), CommandError>;
    fn apply_modes(&self, steamrun_config: &SteamrunConfig) -> io::Result<()>;

    #[inline]
    fn apply_with_modes(&self, steamrun_config: &SteamrunConfig) -> io::Result<()> {
        steamrun_config.steamrun_apply()
            .and_then(|_| self.apply_modes(steamrun_config))
    }
}

impl ConfigExt for Config {
    fn run_config(&self, steamrun_config: &SteamrunConfig) -> Result<(), CommandError> {
        if steamrun_config.exec_method != ExecMethod::SubProcess {
            if let Some(p) = steamrun_config.log_directory() {
                warn!("Logs will not be written to {} due to exec_method: {:?}", p.display(), steamrun_config.exec_method);
            }
        }
        self.run_command(steamrun_config.exec_method, steamrun_config.log_directory(), get_cmd_operations(self, steamrun_config))
    }

    fn apply_modes(&self, steamrun_config: &SteamrunConfig) -> io::Result<()> {
        self.modes(steamrun_config.default_modes())
            .into_iter()
            .filter_map(|m| match steamrun_config.modes().get(m) {
                v @ Some(..) => {
                    debug!("Using mode: {}", m);
                    v
                },
                None => {
                    warn!("Skipping unknown mode not present in config: {}", m);
                    None
                },
            })
            .try_for_each(|mode_config| mode_config.steamrun_apply())
    }
}

#[inline(always)]
fn dump_open_options() -> fs::OpenOptions {
    let mut opts = fs::OpenOptions::new();
    opts
        .write(true)
        .create(true)
        .truncate(true);
    opts
}

fn get_cmd_operations<'c>(config: &Config, steamrun_config: &'c SteamrunConfig) -> Vec<&'c CmdOperation> {
    steamrun_config.cmd_operations(config.modes(steamrun_config.default_modes()))
}

fn dump_env(config: &Config, steamrun_config: &SteamrunConfig) -> io::Result<()> {
    use io::Write;
    if let Some(log_directory) = steamrun_config.log_directory() {
        fs_ext::ensure_dir(log_directory)?;
        let env_path = log_directory.join(format!("{}.env", config.name()));
        let mut env_file = dump_open_options()
            .open(env_path)?;
        for (k, v) in env::vars() {
            writeln!(&mut env_file, "{}={}", k, v)?;
        }
    }
    Ok(())
}

fn dump_cmd(config: &Config, steamrun_config: &SteamrunConfig) -> io::Result<()> {
    use io::Write;
    if let Some(log_directory) = steamrun_config.log_directory() {
        fs_ext::ensure_dir(log_directory)?;
        let cmd_path = log_directory.join(format!("{}.cmd", config.name()));
        let mut cmd_file = dump_open_options()
            .open(cmd_path)?;
        let cmd = config.command_strings(get_cmd_operations(config, steamrun_config))
            .map_err(|e| io::Error::new(io::ErrorKind::Other, format!("error applying command operations: {}", &e)))?;
        for v in cmd.into_iter() {
            writeln!(&mut cmd_file, "{}", v)?;
        }
    }
    Ok(())
}

fn dump_modes(config: &Config, steamrun_config: &SteamrunConfig) -> io::Result<()> {
    use io::Write;
    if let Some(log_directory) = steamrun_config.log_directory() {
        fs_ext::ensure_dir(log_directory)?;
        let modes_path = log_directory.join(format!("{}.modes", config.name()));
        let mut f = dump_open_options()
            .open(modes_path)?;
        config.modes(steamrun_config.default_modes())
            .into_iter()
            .filter(|&m| steamrun_config.modes().get(m).is_some())
            .try_for_each(|mode| writeln!(&mut f, "{}", mode))
    } else {
        Ok(())
    }
}

#[derive(Debug, thiserror::Error)]
enum MainError {
    #[error("Error getting steamrun config: {0}")]
    Config(#[from] ConfigError),
    #[error("Error applying steamrun config: {0}")]
    Application(io::Error),
    #[error("Failed while running command: {0}")]
    Command(#[from] CommandError),
}

fn real_main(config: &Config) -> Result<(), MainError> {
    debug!("config: {:?}", config);
    if config.is_verbose() {
        use io::Write;
        let mut stderr = io::stderr();
        let _result = writeln!(&mut stderr, "steamrun: verbose mode");
    }
    let steamrun_config = config.config()
        .map_err(MainError::from)?;
    debug!("steamrun config: {:?}", &steamrun_config);
    config.apply_with_modes(&steamrun_config)
        .map_err(MainError::Application)?;
    util::or_warn(dump_env(&config, &steamrun_config), "Failed to dump env");
    util::or_warn(dump_cmd(&config, &steamrun_config), "Failed to dump cmd");
    util::or_warn(dump_modes(&config, &steamrun_config), "Failed to dump modes");
    config.run_config(&steamrun_config)
        .map_err(MainError::from)
}

fn main() {
    logging::init();
    let config = Config::parse();
    util::handle_exit_error(real_main(&config), None);
}
