use std::{
    error::Error,
    fmt::Display,
    process,
};
/// Iterator that returns `None` if it does not contain an iterator
/// and returns the values from that iterator if it does.
pub struct MaybeIterator<It: Iterator>(Option<It>);

impl<It: Iterator> From<Option<It>> for MaybeIterator<It> {
    #[inline(always)]
    fn from(it: Option<It>) -> Self {
        MaybeIterator(it)
    }
}

impl<It: Iterator> Iterator for MaybeIterator<It> {
    type Item = <It as Iterator>::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.as_mut().and_then(Iterator::next)
    }
}

#[inline(always)]
pub fn or_warn<T, E: Display>(r: Result<T, E>, message: &str) {
    if let Err(e) = r {
        warn!("{}: {}", message, &e);
    }
}

const EXIT_FAILURE: i32 = 1;

#[inline(always)]
pub fn handle_exit_error<T, E: Error>(r: Result<T, E>, exit_code: Option<i32>) {
    let exit_code = exit_code.unwrap_or(EXIT_FAILURE);
    if let Err(e) = r {
        error!("{}", &e);
        process::exit(exit_code);
    }
}
