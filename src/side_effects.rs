pub fn configure<'f, T, F>(f: F) -> impl FnOnce(T) -> T + 'f
where
    F: FnOnce(&mut T) + 'f,
{
    move |mut v| {
        f(&mut v);
        v
    }
}

#[inline]
pub fn side_effect<'f, T, F>(f: F) -> impl FnOnce(T) -> T + 'f
where
    F: FnOnce(&T) + 'f,
{
    move |v| {
        f(&v);
        v
    }
}
