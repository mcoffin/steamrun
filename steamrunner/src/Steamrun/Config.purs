module Steamrun.Config
    ( SteamrunConfig(..)
    , configFromFile
    , configFromFile'
    , defaultModes
    , modes
    ) where

import Prelude
import Control.Monad.Error.Class (throwError)
import Control.Monad.Except.Trans (ExceptT, runExceptT)
import Data.Argonaut.Core (Json)
import Data.Argonaut.Decode.Class (class DecodeJson, decodeJson)
import Data.Argonaut.YAML.Parser (DecodeError, decodeFileYaml)
import Data.Generic.Rep (class Generic)
import Data.Either (either)
import Data.Maybe (Maybe, maybe, fromMaybe)
import Effect.Aff (Aff)
import Foreign.Object (Object, keys)
import Util.Error (throwShown)

newtype SteamrunConfig =
    SteamrunConfig { modes :: Maybe (Object Json)
                   , default_modes :: Maybe (Array String)
                   , log_directory :: Maybe String
                   }

derive instance genericSteamrunConfig :: Generic SteamrunConfig _

instance decodeJsonSteamrunConfig :: DecodeJson SteamrunConfig where
    decodeJson = either throwError pure <$> map SteamrunConfig <$> decodeJson

configFromFile :: String -> ExceptT DecodeError Aff SteamrunConfig
configFromFile = decodeFileYaml decodeJson

configFromFile' :: String -> Aff SteamrunConfig
configFromFile' = (runExceptT <$> configFromFile) >=> either throwShown pure

defaultModes :: SteamrunConfig -> Array String
defaultModes (SteamrunConfig v) = fromMaybe mempty v.default_modes

modes :: SteamrunConfig -> Array String
modes (SteamrunConfig v) = maybe mempty keys v.modes
