module Data.Argonaut.YAML.Parser
    ( yamlParser
    , decodeFromYaml
    , decodeFromJson
    , decodeFileYaml
    , decodeFileJson
    , DecodeError(..)
    ) where

import Prelude
import Data.Argonaut.Core (Json)
import Data.Argonaut.Decode.Error (JsonDecodeError)
import Data.Argonaut.Parser (jsonParser)
import Data.Either (Either, either)
import Data.Function.Uncurried (Fn3, runFn3)
import Control.Monad.Error.Class (class MonadThrow, throwError)
import Control.Monad.Except.Trans (ExceptT)
import Control.Monad.Trans.Class (lift)
import Effect.Aff (Aff)
import Util.FS (readFile)

foreign import yamlParserNative :: ∀ a. Fn3 (String -> a) (Json -> a) String a

yamlParser :: ∀ m. MonadThrow String m => String -> m Json
yamlParser = runFn3 yamlParserNative throwError pure

data DecodeError = ParseError String
                 | DecodeError JsonDecodeError

instance showDecodeError :: Show DecodeError where
    show (ParseError s) = s
    show (DecodeError e) = show e

throwParseError :: ∀ m a. MonadThrow DecodeError m => String -> m a
throwParseError = throwError <$> ParseError

throwDecodeError :: ∀ m a. MonadThrow DecodeError m => JsonDecodeError -> m a
throwDecodeError = throwError <$> DecodeError

decodeGeneric :: ∀ m a. Monad m => (String -> Either String Json) -> (Json -> Either JsonDecodeError a) -> String -> ExceptT DecodeError m a
decodeGeneric parse decode =
    (either throwParseError pure <$> parse) >=> (either throwDecodeError pure <$> decode)

decodeFromYaml :: ∀ m a. Monad m => (Json -> Either JsonDecodeError a) -> String -> ExceptT DecodeError m a
decodeFromYaml = decodeGeneric yamlParser

decodeFromJson :: ∀ m a. Monad m => (Json -> Either JsonDecodeError a) -> String -> ExceptT DecodeError m a
decodeFromJson = decodeGeneric jsonParser

decodeFileGeneric :: ∀ a. (String -> Either String Json) -> (Json -> Either JsonDecodeError a) -> String -> ExceptT DecodeError Aff a
decodeFileGeneric parse decode = (lift <$> readFile) >=> (either throwParseError pure <$> parse) >=> (either throwDecodeError pure <$> decode)

decodeFileYaml :: ∀ a. (Json -> Either JsonDecodeError a) -> String -> ExceptT DecodeError Aff a
decodeFileYaml = decodeFileGeneric yamlParser

decodeFileJson :: ∀ a. (Json -> Either JsonDecodeError a) -> String -> ExceptT DecodeError Aff a
decodeFileJson = decodeFileGeneric jsonParser
