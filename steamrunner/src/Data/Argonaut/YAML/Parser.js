import YAML from 'yaml';

export function yamlParserNative(onError, onSuccess, s) {
    try {
        return onSuccess(YAML.parse(s));
    } catch(e) {
        return onError(e.message);
    }
}
