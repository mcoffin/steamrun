module Node.Process2
    ( currentWorkingDirectory
    , env
    , getEnv
    , getEnvOrDefault
    , setEnv
    ) where

import Prelude
import Data.Argonaut.Core (Json)
import Data.Argonaut.Decode.Class (decodeJson)
import Data.Function.Uncurried (Fn2, runFn2)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Either (either)
import Effect (Effect)
import Foreign.Object (Object)
import Node.Process as P

foreign import currentWorkingDirectory :: Effect String
foreign import setEnvNative :: Fn2 String String (Effect Unit)
foreign import getEnvNative :: String -> Effect Json

getEnv :: String -> Effect (Maybe String)
getEnv key = either (pure Nothing) pure <$> decodeJson <$> getEnvNative key

getEnvOrDefault :: String -> String -> Effect String
getEnvOrDefault key defaultValue = fromMaybe defaultValue <$> getEnv key

setEnv :: String -> String -> Effect Unit
setEnv = runFn2 setEnvNative

env :: Effect (Object String)
env = P.getEnv
