export function currentWorkingDirectory() {
    return process.cwd();
}

export function setEnvNative(key, value) {
    return () => {
        process.env[key] = value;
    };
}

export function getEnvNative(key) {
    return () => process.env[key] || null;
}
