module Steamrunner
    ( main
    ) where

import Prelude
import Control.Monad.Trans.Class (lift)
import Control.Monad.State (StateT, modify_, execStateT)
import Effect (Effect)
import Effect.Aff (Aff, launchAff_, forkAff, joinFiber)
import Effect.Class (liftEffect)
import Effect.Console (log)
import Steamrun.Config (SteamrunConfig(..), configFromFile', modes, defaultModes)
import Data.Array as A
import Data.Argonaut.Core (Json)
import Data.Argonaut.Decode ((.:), (.:?))
import Data.Argonaut.Decode.Class (class DecodeJson, decodeJson)
import Data.Argonaut.Encode.Class (encodeJson)
import Data.Foldable (fold, foldr)
import Data.List.Lazy.Types (List, toList, (:))
import Data.List.Lazy.NonEmpty as NEL
import Data.Either (Either(..), either)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Options (Options, (:=))
import Data.Traversable (traverse)
import Data.Tuple (Tuple(..))
import Foreign.Object (Object, toUnfoldable)
import Foreign.Object as O
import Minimist (MinimistArray(..))
import Minimist as M
import Minimist.Options (MinimistOptions, interpretAsStrings, interpretAsBooleans, aliases, defaults)
import Node.Process2 (currentWorkingDirectory)
import Node.Process (getEnv)
import Node.Stream (pipe)
import Util.Aff (logAff)
import Util.Error (throwShownPrefix)
import Util.Env (processEnvOr, setEnv)
import Util.Path (joinPath, joinPaths, joinPaths')
import Util.FS (createWriteStream, writeFile)
import Util.Dirs (getHomeDir, configDir)
import Util.Stream (awaitStreamEnd)
import ChildProcess (spawn, awaitTerminationChecked', execChecked', childStdout, childStderr, Stdio(..))
import Zenity (zenityChooser)

-- foreign import parseArgsNative :: Effect Json
parseArgsNative :: Effect Json
parseArgsNative =
    encodeJson <$> M.parseArgs' opts where
        opts :: Options MinimistOptions
        opts = fold [ interpretAsStrings := ["mode", "name"]
                    , interpretAsBooleans := Right ["redirect"]
                    , aliases := O.singleton "mode" ["M"]
                    , defaults := O.fromFoldable [ Tuple "mode" $ encodeJson (mempty :: Array Json)
                                                 , Tuple "redirect" $ encodeJson false
                                                 ]
                    ]

runEnvUpdate :: String -> StateT String Effect Unit -> Effect Unit
runEnvUpdate key update = do
    init <- key `processEnvOr` ""
    final <- execStateT update init
    setEnv key final

updatePath :: Effect Unit
updatePath = runEnvUpdate "PATH" addBinDirs where
    addBinDirs :: StateT String Effect Unit
    addBinDirs = do
        homeDir <- lift $ getHomeDir
        _ <- addEntry `traverse` [ joinPaths' (homeDir : ".cargo" : "bin" : mempty)
                                 , homeDir `joinPaths` [".local", "bin"]
                                 ]
        pure unit
            where
                addEntry :: ∀ m. Monad m => String -> StateT String m Unit
                addEntry "" = pure unit
                addEntry s = modify_ (\old -> old <> ":" <> s)

steamrun :: String -> Array String -> Array String -> Maybe String -> Aff Unit
steamrun name modes command maybeLogDir = do
    case maybeLogDir of
      Just logDir -> do
          child <- liftEffect $ spawn { command: "steamrun"
                                     , args: listToArray steamrunArgs
                                     , stdio: { stdin: Inherit, stdout: Pipe, stderr: Pipe }
                                     }
          done <- forkAff $ pure unit <$> awaitTerminationChecked' child
          Tuple stdoutFile stderrFile <- liftEffect do
              let logFile = \s -> logDir `joinPath` (name <> "-steamrun" <> "." <> s <> ".log")
              out <- createWriteStream $ logFile "stdout"
              err <- createWriteStream $ logFile "stderr"
              outS <- (childStdout child) `pipe` out
              errS <- (childStderr child) `pipe` err
              pure $ Tuple outS errS
          doneWriting <- forkAff do
              awaitStreamEnd stdoutFile
              awaitStreamEnd stderrFile
              pure unit
          _ <- joinFiber `traverse` (doneWriting : done : mempty)
          pure unit
      Nothing -> pure unit <$> execChecked' (Tuple "steamrun" (listToArray steamrunArgs))
    pure unit
        where
            commandArgs :: List String
            commandArgs = arrayToList command where
                arrayToList :: ∀ a. Array a -> List a
                arrayToList a = fold $ toList <$> NEL.singleton <$> a
            steamrunArgs :: List String
            steamrunArgs = baseArgs <> modeArgs <> ("--" : commandArgs) where
                baseArgs :: List String
                baseArgs = "--name" : name : "--verbose" : "--no-default-modes" : mempty
                modeArgs :: List String
                modeArgs = foldr (\mode l -> "--mode" : mode : l) mempty modes
            listToArray :: ∀ a. List a -> Array a
            listToArray l = fold $ A.singleton <$> l

newtype Config =
    Config { modes :: Array String
           , name :: String
           , command :: Array String
           , redirect :: Boolean
           }

instance decodeJsonConfig :: DecodeJson Config where
    decodeJson = (decodeJson :: Json -> Either _ (Object Json)) >=> \config -> do
        MinimistArray modes <- fromMaybe mempty <$> config .:? "mode"
        name <- config .: "name"
        command <- fromMaybe ["env"] <$> config .:? "_"
        redirect <- fromMaybe false <$> config .:? "redirect"
        pure $ Config { modes: modes
                      , name: name
                      , command: command
                      , redirect: redirect
                      }

defaultLogDir :: Effect String
defaultLogDir = flip joinPaths [".local", "log"] <$> getHomeDir

writeInfoFiles :: { name :: String, modes :: Array String, command :: Array String, logDirectory :: Maybe String } -> Aff Unit
writeInfoFiles info = do
    logsDir <- case info.logDirectory of
                 Just d -> pure d
                 Nothing -> liftEffect defaultLogDir
    let logFile = \f -> logsDir `joinPath` f
    writeFile (logFile $ info.name <> ".modes") $ fold (info.modes <#> flip (<>) "\n")
    writeFile (logFile $ info.name <> ".cmd") $ fold (flip (<>) "\n" <$> info.command)
    liftEffect currentWorkingDirectory >>= writeFile (logFile $ info.name <> ".cwd")
    dumpEnv (logFile $ info.name <> ".env")
        where
            dumpEnv :: String -> Aff Unit
            dumpEnv p = do
                (e :: List (Tuple String String)) <- toUnfoldable <$> liftEffect getEnv
                let c = fold $ e <#> \(Tuple k v) -> (k <> "=" <> v <> "\n")
                writeFile p c

defaultDimensions :: Tuple Int Int
defaultDimensions = Tuple 1280 720

main' :: Aff Unit
main' = do
    liftEffect $ updatePath
    Config config <- liftEffect parseArgs
    liftEffect do
        logValue "config" config
        logValue "configModes" config.modes
        logValue "name" config.name
    cfgDir <- liftEffect $ configDir
    globalCfg <- configFromFile' $ cfgDir `joinPath` "steamrun.yml"
    specificCfg <- configFromFile' $ cfgDir `joinPaths` ["steamrun", config.name <> ".yml"]
    let availableModes = fold ([globalCfg, specificCfg] <#> modes)
    liftEffect do
        logValue "globalModes" $ modes globalCfg
        logValue "specificModes" $ modes specificCfg
        logValue "defaultModes" $ defaultModes specificCfg
        logValue "availableModes" availableModes
    chosenModes <- zenityChooser
        { title: "Choose modes"
        , columns: Tuple "Enable" "Mode"
        , width: pure defaultWidth
        , height: pure defaultHeight
        } availableModes (defaultModes specificCfg <> config.modes)
    logAff $ "chosenModes: " <> show chosenModes
    logAff $ "command: " <> show config.command
    let (SteamrunConfig specificConfig) = specificCfg
    writeInfoFiles { name: config.name
                   , modes: chosenModes
                   , command: config.command
                   , logDirectory: specificConfig.log_directory
                   }
    logDir <- case specificConfig.log_directory of
                Just d -> pure d
                Nothing -> liftEffect defaultLogDir
    steamrun config.name chosenModes config.command $ if config.redirect then Just logDir else Nothing
    where
        logValue :: ∀ a. (Show a) => String -> a -> Effect Unit
        logValue k v = log $ (k <> ": ") <> show v
        parseArgs :: Effect Config
        parseArgs = parseArgsNative >>= (either (throwShownPrefix "Error parsing arguments") pure <$> decodeJson)
        (Tuple defaultWidth defaultHeight) = defaultDimensions

main :: Effect Unit
main = launchAff_ main'
