module Minimist.Options
    ( MinimistOptions
    , interpretAsStrings
    , interpretAsBooleans
    , aliases
    , defaults
    , stopEarly
    ) where

import Prelude
import Data.Argonaut.Core (Json)
import Data.Argonaut.Encode.Class (encodeJson)
import Data.Either (Either, either)
import Data.Options (Option, opt)
import Data.Functor.Contravariant ((>#<))
import Foreign.Object (Object)

data MinimistOptions

interpretAsStrings :: Option MinimistOptions (Array String)
interpretAsStrings = opt "strings"

interpretAsBooleans :: Option MinimistOptions (Either Boolean (Array String))
interpretAsBooleans = opt "boolean" >#< either encodeJson encodeJson

aliases :: Option MinimistOptions (Object (Array String))
aliases = opt "alias"

defaults :: Option MinimistOptions (Object Json)
defaults = opt "default" >#< map encodeJson

stopEarly :: Option MinimistOptions Boolean
stopEarly = opt "stopEarly"
