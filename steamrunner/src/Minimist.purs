module Minimist
    ( parseArgs
    , parseArgs'
    , argv
    , MinimistArray(..)
    , decodeMinimistArray
    , decodeMinimistArray'
    ) where

import Prelude
import Control.Alt ((<|>))
import Control.Monad.Error.Class (class MonadError, throwError)
import Data.Argonaut.Core (Json)
import Data.Argonaut.Decode.Class (class DecodeJson, decodeJson)
import Data.Argonaut.Decode.Error (JsonDecodeError)
import Data.Either (Either, either)
import Data.Function.Uncurried (Fn2, runFn2)
import Data.Newtype (class Newtype, wrap, unwrap)
import Data.Options (Options, options)
import Effect (Effect)
import Foreign (Foreign)
import Foreign.Object (Object)
import Minimist.Options (MinimistOptions)

foreign import parseArgsForeign :: Fn2 Foreign (Array String) (Object Json)

-- | Parse command line options into an `Object`
parseArgs :: Options MinimistOptions -> Array String -> Object Json
parseArgs = options >>> runFn2 parseArgsForeign

-- | Convenience type for decoding arrays returned by `minimist`
-- | If the value contains a single value, the result will be a singleton of that value
-- | If the value contains an array, the result will be the array
newtype MinimistArray a = MinimistArray (Array a)

derive instance newtypeMinimistArray :: Newtype (MinimistArray a) _

instance decodeJsonMinimistArray :: DecodeJson a => DecodeJson (MinimistArray a) where
    decodeJson v =
        map MinimistArray $ (decodeJson v) <|> (decodeJson v <#> \m -> [m])

instance semigroupMinimistArray :: Semigroup (MinimistArray a) where
    append (MinimistArray fst) (MinimistArray snd) = wrap $ fst <> snd

instance monoidMinimistArray :: Monoid (MinimistArray a) where
    mempty = wrap mempty

decodeMinimistArray :: ∀ v. DecodeJson v => Json -> Either JsonDecodeError (Array v)
decodeMinimistArray = map unwrap <$> (decodeJson :: Json -> Either _ (MinimistArray v))

decodeMinimistArray' :: ∀ v m. MonadError JsonDecodeError m => DecodeJson v => Json -> m (Array v)
decodeMinimistArray' = map unwrap <$> either throwError pure <$> (decodeJson :: Json -> Either _ (MinimistArray v))

foreign import argv :: Effect (Array String)

parseArgs' :: Options MinimistOptions -> Effect (Object Json)
parseArgs' opts = parseArgs opts <$> argv
