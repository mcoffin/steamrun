import _ from 'lodash';
import { spawn } from 'child_process';

export function spawnNative(command, args, opts = {}) {
    return () => spawn(command, args, opts);
}

class ExitStatusError extends Error {
    constructor(code) {
        super(`child process exited with status: ${code}`);
        this.exitStatus = code;
    }

    get isSignal() {
        return _.isString(this.exitStatus);
    }
}

export function exitStatusErrorNative(codeOrSignal) {
    return new ExitStatusError(codeOrSignal);
}

export function awaitTerminationNative(child) {
    return () => new Promise((resolve, reject) => {
        child.on('error', (e) => reject(e));
        child.on('exit', (code, signal) => {
            const exitStatus = (code === null || code === undefined) ? signal : code;
            resolve(exitStatus);
        });
    });
}

export function getNative(o) {
    return (key) => {
        return _.get(o, key);
    };
}
