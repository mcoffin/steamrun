module ChildProcess
    ( spawnAttached
    , spawn
    , Stdio(..)
    , StdioSettings
    , awaitTermination
    , awaitTerminationChecked
    , awaitTerminationChecked'
    , exec
    , execChecked
    , execChecked'
    , isSuccess
    , ExitStatus(..)
    , ChildProcess
    , childStdin
    , childStdout
    , childStderr
    ) where

import Prelude
import Control.Alt ((<|>))
import Control.Monad.Trans.Class (lift)
import Control.Monad.Error.Class (class MonadThrow, throwError)
import Control.Monad.Except.Trans (ExceptT, runExceptT)
import Control.Promise (Promise, toAffE)
import Data.Argonaut.Core (Json)
import Data.Argonaut.Decode.Class (class DecodeJson, decodeJson)
import Data.Argonaut.Encode.Class (encodeJson)
import Data.Function.Uncurried (Fn3, runFn3)
import Effect (Effect)
import Effect.Aff (Aff)
import Effect.Class (liftEffect)
import Effect.Exception (Error)
import Data.Tuple (Tuple(..))
import Data.Either (either)
import Node.Stream (Readable, Writable)
import Util.Error (throwShown)

foreign import data ChildProcess :: Type
foreign import awaitTerminationNative :: ChildProcess -> Effect (Promise Json)
foreign import spawnNative :: Fn3 String (Array String) Json (Effect ChildProcess)
foreign import exitStatusErrorNative :: ∀ a. a -> Error

data ExitStatus = StatusCode Int
                | Signal String

instance decodeJsonExitStatus :: DecodeJson ExitStatus where
    decodeJson v =
        (StatusCode <$> decodeJson v) <|> (Signal <$> decodeJson v)

instance showExitStatus :: Show ExitStatus where
    show (StatusCode code) = show code
    show (Signal signal) = signal

isSuccess :: ExitStatus -> Boolean
isSuccess (StatusCode 0) = true
isSuccess _ = false

decodeExitStatus :: ∀ m. MonadThrow Error m => Json -> m ExitStatus
decodeExitStatus = either throwShown pure <$> decodeJson

exitStatusError :: ExitStatus -> Error
exitStatusError (StatusCode code) = exitStatusErrorNative code
exitStatusError (Signal signal) = exitStatusErrorNative signal

awaitTermination :: ChildProcess -> Aff ExitStatus
awaitTermination = (toAffE <$> awaitTerminationNative) >=> decodeExitStatus

checkStatus :: ∀ m. MonadThrow ExitStatus m => ExitStatus -> m ExitStatus
checkStatus exitStatus =
    if isSuccess exitStatus
       then pure exitStatus
       else throwError exitStatus

throwExitStatusError :: ∀ m a. MonadThrow Error m => ExitStatus -> m a
throwExitStatusError = throwError <$> exitStatusError

awaitTerminationChecked :: ChildProcess -> ExceptT ExitStatus Aff ExitStatus
awaitTerminationChecked =
    (lift <$> awaitTermination) >=> checkStatus

awaitTerminationChecked' :: ChildProcess -> Aff ExitStatus
awaitTerminationChecked' =
    (runExceptT <$> awaitTerminationChecked) >=> either throwExitStatusError pure

data Stdio = Ignore
           | Pipe
           | Inherit

instance showStdio :: Show Stdio where
    show Ignore = "ignore"
    show Pipe = "pipe"
    show Inherit = "inherit"

type StdioSettings = { stdin :: Stdio
                     , stdout :: Stdio
                     , stderr :: Stdio
                     }

spawn :: { command :: String, args :: Array String, stdio :: StdioSettings } -> Effect ChildProcess
spawn opts = runFn3 spawnNative opts.command opts.args $ encodeJson { stdio: stdioSettings opts.stdio } where
    stdioSettings :: StdioSettings -> Array String
    stdioSettings v = show <$> [v.stdin, v.stdout, v.stderr]

spawnAttached :: Tuple String (Array String) -> Effect ChildProcess
spawnAttached (Tuple command args) = spawn { command: command, args: args, stdio: { stdin: Inherit, stdout: Inherit, stderr: Inherit } }

exec :: Tuple String (Array String) -> Aff ExitStatus
exec =
    (liftEffect <$> spawnAttached) >=> awaitTermination

execChecked :: Tuple String (Array String) -> ExceptT ExitStatus Aff ExitStatus
execChecked =
    (liftEffect <$> spawnAttached) >=> awaitTerminationChecked

execChecked' :: Tuple String (Array String) -> Aff ExitStatus
execChecked' =
    (runExceptT <$> execChecked) >=> either throwExitStatusError pure

foreign import getNative :: ∀ a v. a -> String -> v

childStdin :: ∀ w. ChildProcess -> Writable w
childStdin = flip getNative "stdin"
childStdout :: ∀ r. ChildProcess -> Readable r
childStdout = flip getNative "stdout"
childStderr :: ∀ r. ChildProcess -> Readable r
childStderr = flip getNative "stderr"
