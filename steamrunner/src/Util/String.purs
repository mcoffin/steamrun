module Util.String
    ( trimString
    ) where

foreign import trimString :: String -> String
