import { join } from 'path';

export function joinPathNative(paths) {
    return join(...paths);
}

export const joinPath2Native = join;
