module Util.Foldable
    ( contains
    , contains'
    ) where

import Prelude
import Data.Foldable (class Foldable, find)
import Data.Maybe (isJust)

contains :: ∀ a f. Foldable f => Eq a => a -> f a -> Boolean
contains v = isJust <$> find (eq v)

contains' :: ∀ a f. Foldable f => Eq a => f a -> a -> Boolean
contains' = flip contains
