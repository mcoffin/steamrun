module Util.Dirs
    ( configDir
    , configDir'
    , getHomeDir
    , getHomeDir'
    ) where

import Prelude
import Control.Alt ((<|>))
import Control.Monad.Error.Class (throwError)
import Effect (Effect)
import Effect.Exception (error)
import Data.Maybe (Maybe, maybe)
import Util.Env (processEnv')
import Util.Path (joinPath)

configDir' :: Effect (Maybe String)
configDir' = do
    fromHomeEnv <- map (flip joinPath ".config") <$> processEnv' "HOME"
    fromXdgEnv <- processEnv' "XDG_CONFIG_DIR"
    pure $ fromHomeEnv <|> fromXdgEnv

configDir :: Effect String
configDir = configDir' >>= maybe e pure where
    e :: ∀ a. Effect a
    e = throwError $ error "Failed to find config directory"

getHomeDir' :: Effect (Maybe String)
getHomeDir' = do
    homeDirByUser <- map userHomeDir <$> processEnv' "USER"
    homeDirByEnv <- processEnv' "HOME"
    pure $ homeDirByEnv <|> homeDirByUser
        where
            userHomeDir :: String -> String
            userHomeDir "root" = "/root"
            userHomeDir user = "/home" `joinPath` user

getHomeDir :: Effect String
getHomeDir = getHomeDir' >>= maybe e pure where
    e :: ∀ a. Effect a
    e = throwError $ error "Could not find home directory"
