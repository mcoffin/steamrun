module Util.Env
    ( setEnv
    , processEnv
    , processEnv'
    , processEnvOr
    ) where

import Prelude
import Control.Monad.Error.Class (throwError)
import Control.Monad.Except.Trans (ExceptT)
import Control.Monad.Trans.Class (lift)
import Effect (Effect)
import Data.Argonaut.Core (Json)
import Data.Argonaut.Decode.Class (decodeJson)
import Data.Argonaut.Decode.Error (JsonDecodeError)
import Data.Maybe (Maybe)
import Data.Either (either)
import Node.Process2 (getEnv, getEnvOrDefault)
import Node.Process2 as P

foreign import processEnvNative :: String -> Effect Json

setEnv :: String -> String -> Effect Unit
setEnv = P.setEnv

processEnv :: String -> ExceptT JsonDecodeError Effect (Maybe String)
processEnv = (lift <$> processEnvNative) >=> (either throwError pure <$> decodeJson)

processEnv' :: String -> Effect (Maybe String)
processEnv' = getEnv

processEnvOr :: String -> String -> Effect String
processEnvOr = getEnvOrDefault
