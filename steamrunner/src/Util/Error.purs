module Util.Error
    ( throwShown
    , throwShown'
    , throwShownPrefix
    ) where

import Prelude
import Control.Monad.Error.Class (class MonadThrow, throwError)
import Effect.Exception (Error, error)

throwShown :: ∀ m a v. Show a => MonadThrow Error m => a -> m v
throwShown = throwError <$> error <$> show

throwShown' :: ∀ m a v. Show a => MonadThrow String m => a -> m v
throwShown' = throwError <$> show

throwShownPrefix :: ∀ m a v. Show a => MonadThrow Error m => String -> a -> m v
throwShownPrefix pfx = throwError <$> error <$> buildMessage where
    buildMessage :: a -> String
    buildMessage = flip (<>) (pfx <> ": ") <$> show
