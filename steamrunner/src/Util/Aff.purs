module Util.Aff
    ( logAff
    ) where

import Prelude
import Effect.Aff (Aff)
import Effect.Console (log)
import Effect.Class (liftEffect)

logAff :: String -> Aff Unit
logAff = liftEffect <$> log
