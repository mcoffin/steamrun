export function awaitStreamEndNative(endEvents = ['end']) {
    return (s) => {
        return () => new Promise((resolve, reject) => {
            s.on('error', (e) => reject(e));
            s.on('end', () => resolve());
        });
    };
}

export function streamWriteNative(stream, s, enc) {
    return () => new Promise((resolve, reject) => {
        if (!stream.write(s, enc)) {
            stream.once('drain', resolve);
        } else {
            resolve();
        }
    });
}

function streamToString(s) {
    return new Promise((resolve, reject) => {
        let buf = '';
        s.on('error', (e) => reject(e));
        s.on('data', (chunk) => {
            if (chunk instanceof Buffer) {
                buf = buf + chunk.toString('utf8');
            } else {
                buf = buf + chunk;
            }
        });
        s.on('end', () => resolve(buf));
    });
}

export function streamToStringNative(s) {
    return () => streamToString(s);
}

export function streamEnd(stream) {
    return () => stream.end(Buffer.from(''));
}
