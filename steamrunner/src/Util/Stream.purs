module Util.Stream
    ( awaitStreamEnd
    , awaitStreamEnd'
    , streamWrite
    , streamEnd
    , streamToString
    ) where

import Prelude
import Control.Promise (Promise, toAffE)
import Data.Function.Uncurried (Fn3, runFn3)
import Effect (Effect)
import Effect.Aff (Aff)
import Node.Stream (Stream, Writable, Readable)
import Node.Encoding (Encoding, encodingToNode)

foreign import streamWriteNative :: ∀ w. Fn3 (Writable w) String String (Effect (Promise Unit))
foreign import streamEnd :: ∀ w. Writable w -> Effect Unit
foreign import awaitStreamEndNative :: ∀ w. Array String -> Stream w -> Effect (Promise Unit)
foreign import streamToStringNative :: ∀ r. Readable r -> Effect (Promise String)

streamToString :: ∀ r. Readable r -> Aff String
streamToString = toAffE <$> streamToStringNative

streamWrite :: ∀ w. Writable w -> String -> Encoding -> Aff Unit
streamWrite stream s = toAffE <$> runFn3 streamWriteNative stream s <$> encodingToNode

awaitStreamEnd :: ∀ w. Stream w -> Aff Unit
awaitStreamEnd = toAffE <$> awaitStreamEndNative ["end"]

awaitStreamEnd' :: ∀ w. Array String -> Stream w -> Aff Unit
awaitStreamEnd' events = toAffE <$> awaitStreamEndNative events
