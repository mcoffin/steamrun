module Util.Path
    ( joinPath
    , joinPaths
    , joinPaths'
    ) where

import Prelude
import Data.Foldable (class Foldable, foldl)
import Data.Function.Uncurried (Fn2, runFn2)

foreign import joinPathNative :: Array String -> String
foreign import joinPath2Native :: Fn2 String String String

joinPath :: String -> String -> String
joinPath "" p = p
joinPath p "" = p
joinPath fst snd = runFn2 joinPath2Native fst snd

joinPaths :: String -> Array String -> String
joinPaths fst additional = joinPathNative $ [fst] <> additional

joinPaths' :: ∀ f. Foldable f => f String -> String
joinPaths' = foldl joinPath mempty
