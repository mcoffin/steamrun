export function processEnvNative(key) {
    return () => process.env[key] || undefined;
}
