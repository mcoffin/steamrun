module Util.FS
    ( readFile
    , writeFile
    , createWriteStream
    ) where

import Prelude
import Control.Promise (Promise, toAffE)
import Effect (Effect)
import Effect.Aff (Aff)
import Data.Function.Uncurried (Fn2, runFn2)
import Node.Stream (Writable)

foreign import writeFileNative :: Fn2 String String (Effect (Promise Unit))
foreign import readFileNative :: String -> Effect (Promise String)
foreign import createWriteStream :: ∀ r. String -> Effect (Writable r)

writeFile :: String -> String -> Aff Unit
writeFile path = toAffE <$> runFn2 writeFileNative path

readFile :: String -> Aff String
readFile = toAffE <$> readFileNative
