import fs from 'fs';
import { readFile, writeFile } from 'fs/promises';

export function writeFileNative(...args) {
    return () => writeFile(...args);
}

export function readFileNative(path) {
    return () => readFile(path, { encoding: 'utf8' });
}

export function createWriteStream(p) {
    return () => fs.createWriteStream(p, { encoding: 'utf8' });
}
