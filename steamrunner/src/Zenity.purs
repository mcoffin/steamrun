module Zenity
    ( zenityChooser
    , zenityChooser'
    , ChooserOptions
    ) where

import Prelude
import Effect.Class (liftEffect)
import Effect.Aff (Aff, forkAff, joinFiber)
import Data.Maybe (Maybe, maybe)
import Data.Foldable (fold)
import Data.String (split, Pattern(..))
import Data.Tuple (Tuple(..))
import ChildProcess (ChildProcess, Stdio(..), spawn, awaitTerminationChecked', childStdout)
import Node.Stream (Readable)
import Util.Foldable (contains')
import Util.Stream (streamToString)
import Util.String (trimString)

awaitTerminationChecked'' :: ChildProcess -> Aff Unit
awaitTerminationChecked'' = map (pure unit) <$> awaitTerminationChecked'

type ChooserOptions = { title :: String
                      , columns :: Tuple String String
                      , width :: Maybe Int
                      , height :: Maybe Int
                      }

zenityChooser :: ChooserOptions -> Array String -> Array String -> Aff (Array String)
zenityChooser opts values selectedValues = do
    child <- liftEffect $ spawn { command: "zenity"
                                , args: zenityArgs
                                , stdio: { stdin: Ignore, stdout: Pipe, stderr: Inherit }
                                }
    done <- forkAff $ awaitTerminationChecked'' child
    output <- readOutput $ childStdout child
    pure output <$> joinFiber done
        where
            parseOutput :: String -> Array String
            parseOutput = split (Pattern "|") <$> trimString
            readOutput :: ∀ r. Readable r -> Aff (Array String)
            readOutput = map parseOutput <$> streamToString
            zenityArgs :: Array String
            zenityArgs = initialArgs <> fold (valueArgs <$> values) where
                valueArgs :: String -> Array String
                valueArgs v = [if selectedValues `contains'` v then "TRUE" else "FALSE", v]
                initialArgs :: Array String
                initialArgs =
                    [ "--list", "--checklist"
                    , "--title", opts.title
                    , "--column", selectColumn
                    , "--column", labelColumn
                    ] <> (optFlag "--width" opts.width) <> (optFlag "--height" opts.height)
                        where
                            (Tuple selectColumn labelColumn) = opts.columns
                            optFlag :: ∀ a. (Show a) => String -> Maybe a -> Array String
                            optFlag k = maybe [] (\v -> [k, v]) <$> map show

zenityChooser' :: ChooserOptions -> Array String -> Aff (Array String)
zenityChooser' opts = flip (zenityChooser opts) mempty
